Source: pdcurses
Section: libs
Priority: optional
Maintainer: Agustin Martin Domingo <agmartin@debian.org>
Build-Depends:
 debhelper-compat (= 12),
 quilt (>= 0.40),
 dpkg-dev (>= 1.16.1.1),
 libxt-dev,
 libxaw7-dev,
 libxpm-dev,
 libsdl2-dev
Standards-Version: 4.5.0
Homepage: http://pdcurses.sourceforge.net/
Vcs-Browser: https://github.com/wmcbrine/PDCurses
Vcs-Git: https://github.com/wmcbrine/PDCurses.git

Package: libxcurses-dev
Section: libdevel
Architecture: any
Multi-Arch: same
Depends: pdcurses-devel-common,
 libxcurses3 (= ${binary:Version}),
 ${misc:Depends}
Description: Port of System VR4 curses for X11: Development libraries
 PDCurses is a public domain curses library for X11, Win32, DOS, OS/2
 and SDL, with most functions available in System V R4 curses.
 It supports most compilers on the above operating systems.
 .
 This package contains the development libraries for the X11 port.
 Header files are in pdcurses-devel-common package.

Package: libxcurses3
Architecture: any
Multi-Arch: same
Pre-Depends: ${misc:Pre-Depends}
Breaks: xbase (<< 3.3.2.3a-2)
Depends: ${shlibs:Depends},
 ${misc:Depends}
Description: Port of System VR4 curses for X11: Runtime libraries
 PDCurses is a public domain curses library for X11, Win32, DOS, OS/2
 and SDL, with most functions available in System V R4 curses.
 It supports most compilers on the above operating systems.
 .
 This package contains the runtime libraries for the X11 port.

Package: libpdcurses-dev
Section: libdevel
Architecture: any
Multi-Arch: same
Depends: pdcurses-devel-common,
 ${misc:Depends}
Description: Port of System VR4 curses for SDL: Development libraries
 PDCurses is a public domain curses library for X11, Win32, DOS, OS/2
 and SDL, with most functions available in System V R4 curses.
 It supports most compilers on the above operating systems.
 .
 This package contains the development static libraries for the SDL port.
 Header files are in pdcurses-devel-common package.

Package: pdcurses-devel-common
Section: libdevel
Architecture: all
Multi-Arch: foreign
Depends: ${misc:Depends}
Recommends: libxcurses-dev | libpdcurses-dev
Breaks: pdcurses-headers,
 libxcurses-dev (<= 3.4-4)
Replaces: pdcurses-headers
Description: Port of System VR4 curses for different platforms: Header files
 PDCurses is a public domain curses library for X11, Win32, DOS, OS/2
 and SDL, with most functions available in System V R4 curses.
 It supports most compilers on the above operating systems.
 .
 This package contains the common development headers, needed by both
 libxcurses-dev and libpdcurses-dev. It will also be the place for any
 common development stuff.
