Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: PDCurses
Source: https://github.com/wmcbrine/PDCurses

Files: *
Copyright: 1987-1990 Bjorn Larsson,
           1990-1002 John 'Frotz' Fa'atuai <frotz@dri.com>,
	   1992-2005 Mark Hessling <mark@rexx.org>,
	   2005-2019 William McBrine <wmcbrine@gmail.com>
License: public-domain
 Although the core PDCurses package is Public Domain, mention of
 PDCurses in the documentation of packages linked with it is greatly
 appreciated.

Files: x11/scrlbox.c x11/scrlbox.h
Copyright: 1989 O'Reilly and Associates, Inc
License: MIT-X

Files: debian/*
Copyright: 2015-2018 Agustin Martin Domingo <agmartin@debian.org>
License: GPL-2+

License: GPL-2+
 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.
 .
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 You should have received a copy of the GNU General Public License along
 with this program; if not, see
 <https://www.gnu.org/licenses/>
 .
 On Debian systems, the complete text of the GNU General
 Public License version 2 can be found in
 "/usr/share/common-licenses/GPL-2".

License: MIT-X
 The X Consortium, and any party obtaining a copy of these files from
 the X Consortium, directly or indirectly, is granted, free of charge,
 a full and unrestricted irrevocable, world-wide, paid up,
 royalty-free, nonexclusive right and license to deal in this software
 and documentation files (the "Software"), including without
 limitation the rights to use, copy, modify, merge, publish,
 distribute, sublicense, and/or sell copies of the Software, and to
 permit persons who receive copies from any such party to do so.  This
 license includes without limitation a license to do the foregoing
 actions under any patents of the party supplying this software to the
 X Consortium.
